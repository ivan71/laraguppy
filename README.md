# Documentação

## Instalação

### Siga as etapas abaixo para configurar seu pacote LaraGuppy:

>**Estes são pré-requisitos para o LaraGuppy.**
>* PHP: 8.2.x
>* MySQL: 8.x
>* Laravel: 10.x, 11.x \
>
>Este pacote assume que o Laravel Breeze ou autenticação está habilitado em seu aplicativo Laravel.

1. Pacotes para instalação.

**Instalar o Reverb**

```php
composer require laravel/reverb:@dev
```
Você precisa executar os seguintes comandos no diretório raiz do sistema por meio da **CLI**
```php
composer config repositories.laraguppy '{"type": "vcs", "url": "https://gitlab.com/ivan71/laraguppy.git", "options":{"symlink": false}}' --file composer.json && composer config minimum-stability dev --file composer.json
```

```php
composer require ivan71/laraguppy:dev-main
```

```php
php artisan vendor:publish --provider="ivan71\LaraGuppy\LaraGuppyServiceProvider" --tag=config
```

```php
php artisan vendor:publish --provider="ivan71\LaraGuppy\LaraGuppyServiceProvider" --tag=assets --force
```

2. Você precisa definir as configurações do LaraGuppy no arquivo config/laraguppy.php **(por exemplo; db_prefix, url_prefix route_middleware, etc).**

* Se o seu aplicativo Laravel estiver rodando na versão **11.x** e você ainda não instalou o Laravel Sanctum,
use o seguinte comando na **CLI** para instalá-lo.

```php
php artisan install:api
```

* Importe o rotas HasApiTokens em sua classe Authenticatable, por exemplo; Model User: **use Laravel\Sanctum\HasApiTokens;**
* Adicione esta linha à sua classe Authenticatablepor exemplo; Model User **use HasApiTokens;**
* Após as configurações, você precisa executar este comando na **CLI**

```php
php artisan migrate
```

3. Depois disso, você precisa adicionar as rotas Chatable em sua classe Authenticatable, por exemplo, **User Model.**

* Importe a característica Chatable na classe Authenticatable, por exemplo, User Model. **use ivan71\LaraGuppy\Traits\Chatable**
* Adicione esta linha à sua classe Authenticatable , por exemplo; Model User **use Chatable;**

# Notificações em tempo real

## LaraGuppy fornece notificações em tempo real através da integração com Pusher e Laravel Reverb.

Para ativar o pusher, atualize o arquivo .env do seu aplicativo da seguinte maneira:

>BROADCAST_DRIVER=pusher \
>PUSHER_APP_ID=**** \
>PUSHER_APP_KEY=**** \
>PUSHER_APP_SECRET=**** \
>PUSHER_APP_CLUSTER=**** 

# Reverb Laravel

Para Laravel Reverb execute isto: 

```php
php artisan reverb:install
```

Depois, certifique-se de que o Laravel Reverb adicionou as seguintes configurações à configuração do arquivo .env.

>BROADCAST_DRIVER=reverb \
> \
>REVERB_APP_ID=**** \
>REVERB_APP_KEY=**** \
>REVERB_APP_SECRET=**** \
>REVERB_HOST=**** \
>REVERB_PORT=**** \
>REVERB_SCHEME=****

# Configuração do LaraGuppy

Após concluir a instalação, você terá a oportunidade de personalizar as configurações do LaraGuppy para atender às suas necessidades. Você encontrará uma abundância de opções para explorar e utilizar em **config/laraguppy.php**

### Configurações de rota e layout

Você pode definir seu prefixo de rotas da web e middlewares para proteger as rotas de API e web de chat.

```php
>.... 
>'url_prefix'                     => '',                 // like admin if you are using it in admin panel. 
>'route_middleware'               => ['auth'],           // route middlewares like auth, role etc. 
>'api_authentication_middleware'  => ['auth:sanctum'],   // auth:api for passport.   
>'layout'                         => 'layouts.app',      // Leave emtpy to load default layout for LaraGuppy massenger 
>'content_yeild'                  => 'content',          // Section variable name that yields from above layout. 
>'style_stack'                    => 'styles',           // Push style variable for style css. 
>'script_stack'                   => 'scripts',          // Push scripts variable for custom js and script files. 
>'redirect_url'                   => '',                 // Add the URL link of redirect button. 
>....
```

### Configurações de banco de dados e Eloquent

Essas configurações permitem que você especifique as colunas e relações para as informações do usuário LaraGuppy do banco de dados. Isso é necessário para informar ao LaraGuppy que o usuário possui campos como nome, imagem e telefone no banco de dados.

```php
>....
>'db_prefix'              => 'lg__',          // Prefix for database tables. 
>'userinfo_relation'      => '',              // If user inforamtion from other than User model please specify relation defined in >user_class.
>'userinfo_relation'      => 'activeProfile', // If user inforamtion from other than User model please specify relation defined in >user_class.
>'user_first_name_column' => 'first_name',    // If you don't have a 'first_name' column, leave it as the default 'name'.
>'user_last_name_column'  => 'last_name',     // If you don't last name columns, leave empty otherwise specify last name column.
>'user_email_column'      => 'email',         // It can be a column/attribute defined in user_class, leave empty if defualt users.
>'user_image_column'      => 'image',         // It can be a column/attribute defined in user_class, please fill only when ?>applicable.
>'user_phone_column'      => 'phone',         // It can be a column/attribute defined in user_class, please fill only when >applicable.
>....
```

### Ativar convite para iniciar bate-papo

Por padrão, está definido como 'true', o que significa que um convite de bate-papo será enviado quando dois usuários tentarem interagir. Se você definir como 'falso', um chat direto será inicializado sem convite.

```php
....
'enable_chat_invitation'         => true,
....
```

### Configurações de guias

Essas configurações permitem especificar quais guias você deseja exibir e o número de registros por página para os resultados de cada guia.

```php
....
'per_page_records'   => '20',                                             // Paginate record of every tab.
'enable_tabs'        => ['private_chat', 'friend_list', 'contact_list'],  // Enable tabs to show on laraguppy chat.
'default_active_tab' => 'private_chat',                                   // Default active tab when chat is loaded on page.
....
```


### Configurações de mensagem

Essas configurações permitem utilizar várias opções e configurações para mensagens.

```php
....
'default_avatar_url'              => '',              // User default avater image url
'delete_message'                  => true,            // Allow delete unseen message option
'default_active_tab'              => 'private_chat',  // Default active tab when chat is loaded on page.
'clear_chat'                      => true,            // Allow clear chat option
'time_format'                     => '12hrs',         // Message time format 12hrs or 24hrs

'location_sharing'                => true,            // Enable location sharing option 
'emoji_sharing'                   => true,            // Enable emoji sharing option 
'voice_sharing'                   => true,            // Enable voice note sharing option 
....
```

### Configurações de mídia

Essas configurações permitem ajustar as extensões e tamanhos de arquivo permitidos para anexos de mensagens.

```php
....
'image_size'                => 5000,                            // Upload image size in "KB"
'image_ext'                 => ['.jpg','.jpeg','.gif','.png'],  // Upload image file allowed extensions
'audio_file_size'           => 10000,                           // Upload audio file size in "KB"
'audio_file_ext'            => ['.mp3','.wav'],                 // Upload audio file allowed extensions
'video_file_size'           => 10000,                           // Upload video file size in "KB"
'video_file_ext'            => ['.mp4','.flv','.3gp'],          // Upload video file allowed extensions
'document_file_size'        => 10000,                           // Upload document file size in "KB"
'document_file_ext'         => ['.pdf','.doc','.txt'],          // Upload video file allowed extensions
'notification_bell_url'     => '',                              // Add the audio file url on new message.
....
```

### Configurações de função

Ao usar o pacote **LARAVEL-PERMISSION do Spatie** , você pode excluir funções específicas passando-as como um array. Deixe a matriz vazia se as funções não estiverem sendo usadas.

```php
....
'exclude_roles'             => ['admin']
....
```

## Recursos do LaraGuppy

* Recurso para envio de solicitações de amizade.
* Opção para bloquear ou desbloquear amigos.
* Iniciação direta de bate-papo sem necessidade de solicitação de amizade.
* Várias guias para bate-papos, amigos e contatos.
* Capacidade de bate-papo individual.
* Sincronização entre vários dispositivos em tempo real.
* Atualizações na lista de contatos em tempo real.
* Exibição do status online do usuário em tempo real.
* Indicador para digitação em tempo real.
* Indicadores de mensagens entregues e visualizadas em tempo real.
* Funcionalidade de pesquisa dentro do aplicativo.
* Indicador da última mensagem nos tópicos de conversa.
* Funcionalidade para fazer upload de vários anexos como fotos, arquivos, locais, vídeos e áudio.
* Capacidade de enviar mensagens de voz.
* Recurso para enviar emojis.
* Capacidade de visualizar e baixar todas as mídias das conversas.
* Função para limpar o histórico de conversas.
* Opções para silenciar notificações nos níveis de conversa e conta.
* Painel de detalhes do usuário, incluindo fotos compartilhadas e opções de limpeza de conversas.
* Design responsivo compatível com todos os dispositivos.
* Personalização de perfis de usuário.
