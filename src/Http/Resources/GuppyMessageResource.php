<?php

namespace ivan71\LaraGuppy\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use ivan71\LaraGuppy\ConfigurationManager;
use ivan71\LaraGuppy\Http\Resources\GuppyAttachmentResource;
use ivan71\LaraGuppy\Http\Resources\GuppyLocationResource;
use ivan71\LaraGuppy\Services\MyUser;

class GuppyMessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {

        $profile = (new MyUser)->extractUserInfo($this->messageable);
        return [
            'userId' => $this->messageable?->id,
            'threadId' => $this->thread?->id,
            'threadType' => $this->thread?->thread_type,
            'friendStatus' => $this->thread->participants->first()?->participant_status,
            'blockedBy' => $this->thread->participants->first()?->blocked_by,
            'name' => $profile['name'],
            'photo' => $profile['photo'],
            'messageId' => $this->id,
            'timeStamp' => $this->timeStamp ?? null,
            'messageType' => $this->message_type,
            'body' => empty($this->deleted_at) ? $this->body : null,
            'parent' => new GuppyMessageResource($this->parentMessage),
            'seenAt' => $this->read?->seen_at,
            'deliveredAt' => $this->delivered?->created_at,
            'deletedAt' => $this->deleted_at,
            'isSender' => $this->messageable?->id == auth()?->user()?->id ?? false,
            'location' => ($this->message_type == ConfigurationManager::MESSAGE_LOCATION ? new GuppyLocationResource($this->attachments->first()) : null),
            'attachments' => empty($this->deleted_at) && $this->message_type != ConfigurationManager::MESSAGE_LOCATION ? GuppyAttachmentResource::collection($this->attachments) : null,
            'notification' => new GuppyNotificationResource($this->notification),
            'createdAt' => $this->created_at,
        ];
    }
}
