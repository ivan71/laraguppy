<?php

namespace ivan71\LaraGuppy\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GuppyAttachmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file' => !empty($this->attachments['file']) ? asset('storage/' . $this->attachments['file']) : null,
            'fileName' => $this->attachments['fileName'] ?? null,
            'thumbnail' => !empty($this->attachments['thumbnail']) ? asset('storage/' . $this->attachments['thumbnail']) : null,
            'fileSize' => !empty($this->attachments['fileSize']) ? number_format($this->attachments['fileSize'] / 1024, 2) . ' KB' : null,
            'fileType' => $this->attachments['fileType'] ?? null,
            'latitude' => $this->attachments['latitude'] ?? null,
            'longitude' => $this->attachments['longitude'] ?? null,
        ];

    }
}
