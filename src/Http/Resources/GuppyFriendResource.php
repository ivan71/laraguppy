<?php

namespace ivan71\LaraGuppy\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use ivan71\LaraGuppy\Services\MyUser;

class GuppyFriendResource extends JsonResource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request): mixed
    {

        $profile = (new MyUser)->extractUserInfo($this);
        return [
            'isOnline' => $this->isOnline,
            'userId' => $this->id,
            'threadId' => $this->threads->first()?->id,
            'name' => $profile['name'],
            'photo' => $profile['photo'],
            'threadType' => 'private',
            'blockedBy' => $this->pivot?->blocked_by,
            'friendStatus' => $this->pivot?->friend_status,
        ];
    }
}
