<?php

namespace ivan71\LaraGuppy\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ivan71\LaraGuppy\Http\Requests\ProfileStoreRequest;
use ivan71\LaraGuppy\Http\Resources\GuppyContactsResource;
use ivan71\LaraGuppy\Http\Resources\GuppySettingsResource;
use ivan71\LaraGuppy\Http\Resources\GuppyUserResource;
use ivan71\LaraGuppy\Models\GpUser;
use ivan71\LaraGuppy\Services\ChatService;
use ivan71\LaraGuppy\Traits\ApiResponser;

class ProfileController extends Controller
{
    use ApiResponser;
    /**
     * Get user profile
     */
    public function index(Request $request)
    {
        $model = get_class($request->user());
        $request->request->add(['model_class' => $model]);
        return response()->json(['type' => 'success', 'user' => new GuppyUserResource($request->user())]);
    }

    /**
     * Edit user profile
     */
    public function store(ProfileStoreRequest $request)
    {
        $values = $request->only(['name', 'email', 'phone']);
        $data = array_filter($values);
        if ($request->hasFile('photo')) {
            $file_path = $request->file('photo')->store('public/laraguppy/profile');
            $file_path = str_replace('public/', '', $file_path);
            $data['photo'] = $file_path;
        }

        GpUser::updateOrCreate(['user_id' => auth()->user()->id], $data);

        return $this->success(new GuppyUserResource($request->user()), __('laraguppy::chatapp.profile_updated'));
    }

    /**
     * Get LaraGuppy Settings
     */
    public function settings(Request $request)
    {
        return $this->success(new GuppySettingsResource($request), );
    }

    /**
     * Display a listing of the contacts.
     */
    public function contacts()
    {
        $contacts = [];

        $contacts = (new ChatService)->getContacts();

        return response()->json(['type' => 'success', 'data' => new GuppyContactsResource($contacts)]);
    }

    /**
     * Display a listing of the contacts.
     */
    public function reportUser(Request $request)
    {

    }
}
