<?php

namespace ivan71\LaraGuppy\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use ivan71\LaraGuppy\ConfigurationManager;
use Laravel\Sanctum\HasApiTokens;

class GuestAccount extends Authenticatable
{
    use HasFactory, HasApiTokens, SoftDeletes;

    protected $table;

    protected $guarded = [];

    public function __construct()
    {
        $this->table = config('laraguppy.db_prefix') . ConfigurationManager::GUEST_ACCOUNTS_TABLE;
        parent::__construct();
    }
}
