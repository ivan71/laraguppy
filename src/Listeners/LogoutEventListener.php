<?php

namespace ivan71\LaraGuppy\Listeners;

use IlluminateAuthEventsLogout;
use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use ivan71\LaraGuppy\ConfigurationManager;
use ivan71\LaraGuppy\Events\GuppyChatPublicEvent;
use ivan71\LaraGuppy\Http\Resources\GuppyUserResource;

class LogoutEventListener
{

    /**
     * Handle the event.
     *
     * @param  \IlluminateAuthEventsLogout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        Cache::forget('user-online-' . $event->user->id);
        Cookie::queue('guppy_auth_token', '', -1);
        broadcast(new GuppyChatPublicEvent(new GuppyUserResource($event->user), ConfigurationManager::UserOfflineEvent))
            ->toOthers();
    }
}
