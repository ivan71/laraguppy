<?php

namespace ivan71\LaraGuppy\Listeners;

use IlluminateAuthEventsLogout;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use ivan71\LaraGuppy\ConfigurationManager;
use ivan71\LaraGuppy\Events\GuppyChatPublicEvent;
use ivan71\LaraGuppy\Http\Resources\GuppyUserResource;

class LoginEventListener
{

    /**
     * Handle the event.
     *
     * @param  \IlluminateAuthEventsLogout  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->user->tokens()->delete();
        $token = $event->user->createToken('GuppyChat')->plainTextToken;
        Cookie::queue('guppy_auth_token', $token, 60 * 24 * 30);
        broadcast(new GuppyChatPublicEvent(new GuppyUserResource($event->user), ConfigurationManager::UserOnlineEvent))
            ->toOthers();
    }
}
